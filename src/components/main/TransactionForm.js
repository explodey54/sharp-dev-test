import * as React from 'react';
import withStyles from '@material-ui/core/styles/withStyles';
import FormControl from '@material-ui/core/FormControl';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import Button from '@material-ui/core/Button';
import MenuList from '@material-ui/core/MenuList';
import MenuItem from '@material-ui/core/MenuItem';
import Paper from '@material-ui/core/Paper';
import FormHelperText from '@material-ui/core/FormHelperText';

const styles = theme => ({
	autocomplete: {
		position: 'absolute',
		top: '100%',
    	zIndex: 100
	},
	formControl: {
		position: 'relative'
	},
	drawer: {
	    flexShrink: 0,
  	},
  	flex: {
  		display: 'flex',
  		alignItems: 'center'
  	},
  	button: {
  		marginTop: theme.spacing.unit
  	}
});

class TransactionForm extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			nameFocus: false,
			nameLock: false
		}

		this.handleSubmit.bind(this)
        this.handleAutocomplete.bind(this)
	}

	handleChange = (e) => {
		this.props.setForm({
            [e.target.name]: e.target.value
        })
        this.props.clearErrors()
	}

	handleSubmit = (e) => {
        e.preventDefault()
        this.props.createTransaction(this.props.form.name, this.props.form.amount)
        this.props.clearErrors()
    }

    handleAutocomplete = (e) => {
        this.props.setForm({
            [e.target.name]: e.target.value
        })
        this.props.searchUser(e.target.value)
    }

    renderAutocomplete = (arr) => {
    	return (
    		<Paper className={this.props.classes.autocomplete}>
    			<MenuList
	          	open={arr.length > 0}>
	    		{arr.map((item, index) => (
					<MenuItem key={item.id} 
							  onMouseDown={() => {
							  	this.setState({nameLock: true})
							  	this.props.setForm({
						            name: item.name
						        })
							  }}>{item.name}</MenuItem>
				))}
				</MenuList>
    		</Paper>
    		
		);
    }

	render() {
		return (
			<form onSubmit={this.handleSubmit}>
			    <FormControl margin="normal" className={this.props.classes.formControl} error={Boolean(this.props.errors.length)} fullWidth required>
			        <InputLabel htmlFor="name">Name</InputLabel>
			        <Input id="name" name="name" autoComplete="off"
			        value={this.props.form.name}
			        onChange={this.handleAutocomplete} 
			        onFocus={() => {
			        	this.setState({nameFocus: true})
			        	this.props.clearErrors()
			        }}
			        onBlur={() => {this.setState({nameFocus: false})}} />
			        {this.props.errors.map((item, index) => (
                        <FormHelperText key={index}>{item}</FormHelperText>
                    ))}
			        {	
			        	this.props.searchUserArray && this.props.form.name && this.state.nameFocus && !this.state.nameLock &&
			        	this.renderAutocomplete(this.props.searchUserArray)
			        }
			    </FormControl>
			    <FormControl margin="normal" className={this.props.classes.formControl} fullWidth required>
			        <InputLabel htmlFor="amount">Amount</InputLabel>
			        <Input id="amount" name="amount" autoComplete="amount" 
			        	   value={this.props.form.amount}
			        	   onChange={this.handleChange}
			        	   onFocus={() => {this.props.clearErrors()}}/>
			    </FormControl>
			    <Button
			      type="submit"
			      fullWidth
			      variant="contained"
			      color="primary"
			      className={this.props.classes.button}>
			      Create
			    </Button>
			</form>
		);
	}
}

export default withStyles(styles)(TransactionForm);
