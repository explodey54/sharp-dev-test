import * as React from 'react';
import Divider from '@material-ui/core/Divider';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Paper from '@material-ui/core/Paper';

import withStyles from '@material-ui/core/styles/withStyles';

import Balance from './Balance';
import TransactionForm from './TransactionForm';

const styles = theme => ({
	drawer: {
	    flexShrink: 0,
  	},
  	flex: {
  		display: 'flex',
  		alignItems: 'center'
  	},
  	paperForm: {
  		padding: theme.spacing.unit * 2,
  		margin: theme.spacing.unit * 2
  	},
  	sidebar: {
  		width: 400
  	}
});

class Sidebar extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			name: '',
			amount: null
		}

		this.handleSubmit.bind(this)
        this.handleChange.bind(this)
	}

	handleSubmit = (e) => {
        e.preventDefault()
        this.props.createTransaction(this.state.name, this.state.amount)
    }

    handleChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

	render() {
		return (
			<div className={this.props.classes.sidebar}>
		      	<Balance name={this.props.name} balance={this.props.balance}/>
		      	<Divider/>
		      	<List component="nav">
					<ListItem button selected={this.props.isShownTransactionForm} onClick={this.props.toggleTransactionForm}>
						<ListItemText primary="Create new transaction" />
					</ListItem>
					{
						this.props.isShownTransactionForm && 
						<Paper className={this.props.classes.paperForm}>
							<TransactionForm
							errors={this.props.errors}
                    		clearErrors={this.props.clearErrors}
							createTransaction={this.props.createTransaction}
							searchUser={this.props.searchUser} 
							searchUserArray={this.props.searchUserArray}
							removeRepeatTransactionInfo={this.props.removeRepeatTransactionInfo}
							form={this.props.form}
							setForm={this.props.setForm}/>
						</Paper>
					}
					<ListItem button>
						<ListItemText primary="Log out" onClick={this.props.logOut}/>
					</ListItem>
		      	</List>
      		</div>
		);
	}
}

export default withStyles(styles)(Sidebar);