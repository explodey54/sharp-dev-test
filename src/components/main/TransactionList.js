import * as React from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Button from '@material-ui/core/Button';

class TransactionList extends React.Component {
	render() {
		return (
			<Table>
				<TableHead>
					<TableRow>
						<TableCell>Date</TableCell>
						<TableCell>Name</TableCell>
						<TableCell numeric>Transaction amount (PW)</TableCell>
						<TableCell numeric>Resulting balance (PW)</TableCell>
						<TableCell></TableCell>
					</TableRow>
				</TableHead>
				<TableBody>
					{this.props.transactions.map((item) => {
						return (
							<TableRow key={item.id}>
								<TableCell>{item.date}</TableCell>
								<TableCell>{item.username}</TableCell>
								<TableCell numeric style={{color: item.amount >= 0 ? 'green' : 'red'}}>{item.amount > 0 && '+'}{item.amount}</TableCell>
								<TableCell numeric>{item.balance}</TableCell>
								<TableCell>
									{
										item.amount < 0 && 
										<Button 
											onClick={() => {
												this.props.showTransactionForm()
												this.props.setForm({name: item.username, amount: String(item.amount)})
											}}>
											Repeat
										</Button>
									}
								</TableCell>
							</TableRow>
							);
					})}
				</TableBody>
			</Table>
		);
	}
}

export default TransactionList;