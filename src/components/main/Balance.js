import * as React from 'react';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import withStyles from '@material-ui/core/styles/withStyles';

const styles = theme => ({
	name: {
		display: 'flex',
		alignItems: 'center',
	},
	balance: {
		padding: theme.spacing.unit * 3,
		paddingBottom: theme.spacing.unit,
		backgroundColor: '#F5F5F5'
	},
	avatar: {
		backgroundColor: theme.palette.secondary.main,
		marginRight: theme.spacing.unit
	}
});

class Balance extends React.Component {
	getAvatarLetter(name) {
		try {
			return name.charAt(0).toUpperCase()
		} catch (e) {
			return ' '
		}
	}

	render() {
		return (
			<div className={this.props.classes.balance}>
		      	<div className={this.props.classes.name}>
		      		<Avatar className={this.props.classes.avatar}>{this.getAvatarLetter(this.props.name)}</Avatar>
		      		<Typography variant="h6">{this.props.name ? this.props.name : '...'}</Typography>
		      	</div>
		      	<Typography variant="subtitle1" gutterBottom>Balance: {this.props.balance} PW</Typography>
	      	</div>
		);
	}
}

export default withStyles(styles)(Balance);