import * as React from 'react';
import withStyles from '@material-ui/core/styles/withStyles';
import FormControl from '@material-ui/core/FormControl';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import FormHelperText from '@material-ui/core/FormHelperText';

const styles = theme => ({
    form: {
        display: 'flex',
        flexDirection: 'column'
    },
    button: {
        marginTop: theme.spacing.unit * 2,
        fontWeight: 'bold'
    }
});

class LoginForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            email : '',
            password: ''
        }

        this.handleSubmit.bind(this)
        this.handleChange.bind(this)
    }

    handleSubmit = (e) => {
        e.preventDefault()
        this.props.login(this.state.email, this.state.password)
        this.props.clearErrors()
    }

    handleChange = (e) => {
        this.props.clearErrors()
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    render() {
        return (
            <form className={this.props.classes.form} onSubmit={this.handleSubmit} method="post">
                <Typography variant="h5" gutterBottom align="center">
                    Sign In
                </Typography>
                <FormControl margin="normal" error={Boolean(this.props.errors.length)} fullWidth required>
                    <InputLabel htmlFor="email">Email Address</InputLabel>
                    <Input id="email" name="email" autoComplete="email" autoFocus type="email" 
                    onChange={this.handleChange}
                    onFocus={() => {this.props.clearErrors()}} 
                    />
                    {this.props.errors.map((item, index) => (
                        <FormHelperText key={index}>{item}</FormHelperText>
                    ))}
                </FormControl>
                <FormControl margin="normal"  fullWidth required>
                    <InputLabel htmlFor="password">Password</InputLabel>
                    <Input id="password" name="password" autoComplete="current-password" type="password"
                           onChange={this.handleChange} 
                           onFocus={() => {this.props.clearErrors()}} />
                </FormControl>
                <Button
                  type="submit"
                  fullWidth
                  variant="contained"
                  color="primary"
                  className={this.props.classes.button}
                >
                  Sign in
                </Button>
                <Button
                  fullWidth
                  variant="contained"
                  color="primary"
                  className={this.props.classes.button}
                  onClick={() => {this.props.changeShownComp(['register'])}}
                >
                  Register
                </Button>
            </form>
        );
    }
}

export default withStyles(styles)(LoginForm);