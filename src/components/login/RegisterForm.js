import * as React from 'react';
import withStyles from '@material-ui/core/styles/withStyles';
import FormControl from '@material-ui/core/FormControl';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import FormHelperText from '@material-ui/core/FormHelperText';

const styles = theme => ({
    form: {
        display: 'flex',
        flexDirection: 'column'
    },
    button: {
        marginTop: theme.spacing.unit * 2,
        fontWeight: 'bold'
    }
});

class RegisterForm extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			username: '',
			email: '',
			password: ''
		}

        this.handleSubmit.bind(this)
        this.handleChange.bind(this)
	}

    handleSubmit = (e) => {
        e.preventDefault()
        this.props.createUser(this.state.username, this.state.password, this.state.email)
        this.props.clearErrors()
    }

    handleChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

	render() {
		return (
			<form className={this.props.classes.form} onSubmit={this.handleSubmit} method="post">
                <Typography variant="h5" gutterBottom align="center">
                    Register
                </Typography>
                <FormControl margin="normal"  fullWidth required>
                    <InputLabel htmlFor="username">Username</InputLabel>
                    <Input id="username" name="username" autoComplete="username" autoFocus 
                    onChange={this.handleChange}
                    onFocus={() => {this.props.clearErrors()}}/>
                </FormControl>
                <FormControl margin="normal" error={Boolean(this.props.errors.length)} fullWidth required>
                    <InputLabel htmlFor="email">Email Address</InputLabel>
                    <Input id="email" name="email" autoComplete="email" type="email" 
                    onChange={this.handleChange}
                    onFocus={() => {this.props.clearErrors()}} 
                    />
                    {this.props.errors.map((item, index) => (
                        <FormHelperText key={index}>{item}</FormHelperText>
                    ))}
                </FormControl>
                <FormControl margin="normal" fullWidth required>
                    <InputLabel htmlFor="password">Password</InputLabel>
                    <Input id="password" name="password" autoComplete="current-password" type="password" 
                    onChange={this.handleChange}
                    onFocus={() => {this.props.clearErrors()}}
                    />
                </FormControl>
                <Button
                    type="submit"
                    fullWidth
                    variant="contained"
                    color="primary"
                    className={this.props.classes.button}
                    >
                        Register
                </Button>
                <Button
                    fullWidth
                    color="primary"
                    className={this.props.classes.button}
                    onClick={() => {this.props.changeShownComp(['login'])}}>
                    Go back to Login
                </Button>
            </form>
			);
	}
}

export default withStyles(styles)(RegisterForm);