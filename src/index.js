import * as React from 'react';
import * as ReactDOM from 'react-dom';
import App from './App';
import './style.css';
import { Provider } from 'react-redux';
import configureStore from './redux/store/configure-store'

const store = configureStore()

ReactDOM.render(
	<Provider store={store}>
		<App />
	</Provider>,
    document.getElementById('root')
);
