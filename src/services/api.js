const url = 'http://193.124.114.46:3001'

export function createUserFetch(username, password, email) {
	return fetch(`${url}/users`, {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({username: username, password: password, email: email,})
	})
}

export function loginFetch(email, password) {
	return fetch(`${url}/sessions/create`, {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({email: email, password: password})
	})
}


export function userInfoFetch(token) {
	return fetch(`${url}/api/protected/user-info`, {
		method: 'GET',
		headers: {
			'Content-Type': 'application/json',
			'Authorization': `Bearer ${token}`
		}
	})
}

export function userTransactionsFetch(token) {
	return fetch(`${url}/api/protected/transactions`, {
		method: 'GET',
		headers: {
			'Content-Type': 'application/json',
			'Authorization': `Bearer ${token}`
		}
	})
}

export function createTransactionFetch(token, name, amount) {
	return fetch(`${url}/api/protected/transactions`, {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json',
			'Authorization': `Bearer ${token}`
		},
		body: JSON.stringify({name: name, amount: amount})
	})
}

export function searchUserFetch(token, filter) {
	return fetch(`${url}/api/protected/users/list`, {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json',
			'Authorization': `Bearer ${token}`
		},
		body: JSON.stringify({filter: filter})
	})
}