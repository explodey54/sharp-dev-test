import * as React from 'react';
import withStyles from '@material-ui/core/styles/withStyles';

import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles'

import LoginContainer from './containers/LoginContainer';
import MainContainer from './containers/MainContainer';
import { connect } from 'react-redux'

const theme = createMuiTheme({
    palette: {
        primary: {
            main: '#039be5',
        },
        secondary: {
            main: '#ff6e40',
        },
    },
    typography: {
        useNextVariants: true,
    },
});

const styles = theme => ({
    app: {
        display: 'flex'
    }
});



const mapStateToProps = (state) => {
    const { shownContainer } = state.core

    return {
    	shownContainer
    }
}


class App extends React.Component {
    render() {
        return (
            <MuiThemeProvider theme={theme}>
                <div className={this.props.classes.app}>
                	{this.props.shownContainer === 'login' && <LoginContainer/>}
                    {this.props.shownContainer === 'main' && <MainContainer/>}
                </div>
            </MuiThemeProvider>
        );
    }
}

App = connect(mapStateToProps)(App);
export default withStyles(styles)(App);