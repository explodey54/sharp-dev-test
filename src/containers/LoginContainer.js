import * as React from 'react';
import { connect } from 'react-redux'

import withStyles from '@material-ui/core/styles/withStyles';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';

import LoginForm from '../components/login/LoginForm';
import RegisterForm from '../components/login/RegisterForm';
import * as loginActions from '../redux/actions/login'

const mapStateToProps = (state) => {
    const { isLoading, isLogged, shownComp, errors } = state.containers.login

    return {
        isLoading,
        isLogged,
        shownComp,
        errors
    }
}

const mapDispatchToProps = {
    clearErrors: loginActions.clearErrors,
    changeShownComp: loginActions.changeShownComp,
    login: loginActions.login,
    createUser: loginActions.createUser
}

const styles = theme => ({
    layout: {
        width: 'auto',
        display: 'block',
        marginLeft: theme.spacing.unit * 3,
        marginRight: theme.spacing.unit * 3,
        [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
            width: 350,
            marginLeft: 'auto',
            marginRight: 'auto',
        },
        padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme.spacing.unit * 3}px`,
        marginTop: theme.spacing.unit * 8
    }
});

class LoginContainer extends React.Component {
    render() {
        return (
            <Paper className={this.props.classes.layout}>
                <Typography variant="h3" gutterBottom align="center">
                    PW App
                </Typography>
                {   
                    this.props.shownComp.indexOf('login') >= 0 && 
                    <LoginForm
                    changeShownComp={this.props.changeShownComp}
                    login={this.props.login}
                    isLoading={this.props.isLoading}
                    errors={this.props.errors.login}
                    clearErrors={this.props.clearErrors}/> 
                }
                {   
                    this.props.shownComp.indexOf('register') >= 0 && 
                    <RegisterForm
                    changeShownComp={this.props.changeShownComp}
                    createUser={this.props.createUser}
                    isLoading={this.props.isLoading}
                    errors={this.props.errors.register}
                    clearErrors={this.props.clearErrors}/> 
                } 
            </Paper>
            );
        }
}

LoginContainer = connect(mapStateToProps, mapDispatchToProps)(LoginContainer);
export default withStyles(styles)(LoginContainer);