import * as React from 'react';
import { connect } from 'react-redux'

import Sidebar from '../components/main/Sidebar';
import TransactionList from '../components/main/TransactionList';

import * as mainActions from '../redux/actions/main'

const mapStateToProps = (state) => {
    const { id, name, email, balance, transactions, searchUserArray, isShownTransactionForm, errors, form } = state.containers.main

    return {
        id,
        name,
        email,
        balance,
        transactions,
        searchUserArray,
        isShownTransactionForm,
        errors,
        form
    }
}

const mapDispatchToProps = {
    clearErrors: mainActions.clearErrors,
    logOut: mainActions.logOut,
    getUserInfo: mainActions.getUserInfo,
    getUserTransactions: mainActions.getUserTransactions,
    createTransaction: mainActions.createTransaction,
    searchUser: mainActions.searchUser,
    showTransactionForm: mainActions.showTransactionForm,
    hideTransactionForm: mainActions.hideTransactionForm,
    toggleTransactionForm: mainActions.toggleTransactionForm,
    setForm: mainActions.setForm
}

class MainContainer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            repeatName: '',
            repeatAmount: ''
        }
    }

    componentDidMount() {
        this.props.getUserInfo()
        this.props.getUserTransactions()
    }

    repeatTransactionCallback = (name, amount) => {
        this.setState({
            repeatName: name,
            repeatAmount: amount
        })
    }

	render() {
		return (
            <React.Fragment>
    			<Sidebar 
                    errors={this.props.errors.transaction}
                    clearErrors={this.props.clearErrors}
                    name={this.props.name} 
                    balance={this.props.balance}
                    createTransaction={this.props.createTransaction}
                    searchUser={this.props.searchUser}
                    searchUserArray={this.props.searchUserArray}
                    logOut={this.props.logOut}
                    showTransactionForm={this.props.showTransactionForm}
                    hideTransactionForm={this.props.hideTransactionForm}
                    toggleTransactionForm={this.props.toggleTransactionForm}
                    isShownTransactionForm={this.props.isShownTransactionForm}
                    form={this.props.form}
                    setForm={this.props.setForm}/>
                <TransactionList 
                    transactions={this.props.transactions}
                    setForm={this.props.setForm}
                    showTransactionForm={this.props.showTransactionForm}/>
            </React.Fragment>
			);
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(MainContainer);