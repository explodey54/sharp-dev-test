import { AUTHORIZATION_FAIL, LOG_OUT } from '../constants/core'
import {
	LOGIN_SUCCESS,
	LOGIN_FAIL,
	CREATE_USER_SUCCESS,
} from '../constants/login'

const initialState = {
    isLogged: false,
    token: null,
    shownContainer: 'login'
}

export default function(state = initialState, action) {
    switch (action.type) {

    	case LOGIN_SUCCESS:
    		return {
                ...state,
                token: action.payload,
                isLogged: true,
                shownContainer: 'main'
            }

        case LOGIN_FAIL:
    		return {
                ...state,
                token: null,
                isLogged: false,
                shownContainer: 'login'
            }

        case CREATE_USER_SUCCESS:
    		return {
                ...state,
                token: action.payload,
                isLogged: true,
                shownContainer: 'main'
            }

        case AUTHORIZATION_FAIL:
        case LOG_OUT:
            return {
                ...state,
                token: null,
                isLogged: false,
                shownContainer: 'login'
            }

        default: 
            return state;
    }
}