import { REMOVE_ERRORS } from '../constants/core'
import {
    LOGIN_REQUEST,
    LOGIN_SUCCESS,
    LOGIN_FAIL,
    CREATE_USER_REQUEST,
    CREATE_USER_SUCCESS,
    CREATE_USER_FAIL,
    CHANGE_SHOWN_COMP
} from '../constants/login'

const initialState = {
    isLoading: false,
    shownComp: ['login'],
    errors: {
        login: [],
        register: []
    }
}

export default function(state = initialState, action) {
    switch (action.type) {
        case CREATE_USER_REQUEST:
            return {
                ...state,
                isLoading: true
            }

        case CREATE_USER_SUCCESS:
            return {
                ...state,
                isLoading: false
            }

        case CREATE_USER_FAIL:
            return {
                ...state,
                isLoading: false,
                errors: {
                    register: [action.payload]
                }
            }

        case LOGIN_REQUEST:
            return {
                ...state,
                isLoading: true
            }

        case LOGIN_SUCCESS:
            return {
                ...state,
                isLoading: false
            }

        case LOGIN_FAIL:
            return {
                ...state,
                isLoading: false,
                errors: {
                    login: [action.payload]
                }
            }

        case CHANGE_SHOWN_COMP:
            return {
                ...state,
                shownComp: action.payload
            }

        case REMOVE_ERRORS:
            return {
                ...state,
                errors: {
                    login: [],
                    register: []
                }
            }

        default: 
            return state;
    }
}
