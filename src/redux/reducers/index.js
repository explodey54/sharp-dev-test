import { combineReducers } from 'redux'
import core from './core'
import login from './login'
import main from './main'


export default combineReducers({
	core,
	containers: combineReducers({login, main})
})
