import { REMOVE_ERRORS } from '../constants/core'
import {
	USER_INFO_REQUEST,
    USER_INFO_SUCCESS,
    USER_INFO_FAIL,
    USER_TRANSACTIONS_REQUEST,
    USER_TRANSACTIONS_SUCCESS,
    USER_TRANSACTIONS_FAIL,
    CREATE_TRANSACTION_REQUEST,
    CREATE_TRANSACTION_SUCCESS,
    CREATE_TRANSACTION_FAIL,
    SEARCH_USER_REQUEST,
    SEARCH_USER_SUCCESS,
    SEARCH_USER_FAIL,
    TRANSACTION_FORM_SHOW,
    TRANSACTION_FORM_HIDE,
    TRANSACTION_FORM_TOGGLE,
    REPEAT_TRANSACTION_SET,
    REPEAT_TRANSACTION_REMOVE,
    SET_FORM
} from '../constants/main'

const initialState = {
    isLoading: false,
    isShownTransactionForm: false,
    id: null,
    name: '',
    email: '',
    balance: null,
    transactions: [],
    searchUserArray: [],
    form: {
        name: '',
        amount: ''
    },
    repeat: {
        name: null,
        amount: null
    },
    errors: {
        transaction: []
    }
}

export default function(state = initialState, action) {
    switch (action.type) {

    	case USER_INFO_REQUEST:
    		return {
                ...state,
                isLoading: true
            }

        case USER_INFO_SUCCESS:
            return {
                ...state,
                isLoading: false,
                id: action.payload.id,
                name: action.payload.name,
                email: action.payload.email,
                balance: action.payload.balance
            }

        case USER_INFO_FAIL:
            return {
                ...state,
                isLoading: false
            }

        case USER_TRANSACTIONS_REQUEST:
            return {
                ...state,
                isLoading: true
            }

        case USER_TRANSACTIONS_SUCCESS:
            return {
                ...state,
                isLoading: false,
                transactions: action.payload
            }

        case USER_TRANSACTIONS_FAIL:
            return {
                ...state,
                isLoading: false
            }

        case CREATE_TRANSACTION_REQUEST:
            return {
                ...state,
                isLoading: true
            }

        case CREATE_TRANSACTION_SUCCESS:
            const copy = [...state.transactions]
            copy.unshift(action.payload)
            return {
                ...state,
                isLoading: false,
                balance: action.payload.balance,
                transactions: copy
            }

        case CREATE_TRANSACTION_FAIL:
            return {
                ...state,
                isLoading: false,
                errors: {
                    transaction: [action.payload]
                }
            }

        case SEARCH_USER_REQUEST:
            return {
                ...state,
                isLoading: true
            }

        case SEARCH_USER_SUCCESS:
            return {
                ...state,
                isLoading: false,
                searchUserArray: action.payload
            }

        case SEARCH_USER_FAIL:
            return {
                ...state,
                isLoading: false
            }

        case TRANSACTION_FORM_SHOW:
            return {
                ...state,
                isShownTransactionForm: true
            }

        case TRANSACTION_FORM_HIDE:
            return {
                ...state,
                isShownTransactionForm: false
            }

        case TRANSACTION_FORM_TOGGLE:
            return {
                ...state,
                isShownTransactionForm: !state.isShownTransactionForm
            }

        case REPEAT_TRANSACTION_SET:
            return {
                ...state,
                repeat: {
                    name: action.payload.name,
                    amount: action.payload.amount,
                }
            }

        case REPEAT_TRANSACTION_REMOVE:
            return {
                ...state,
                repeat: {
                    name: null,
                    amount: null,
                }
            }

        case REMOVE_ERRORS:
            return {
                ...state,
                errors: {
                    transaction: []
                }
            }

        case SET_FORM:
            return {
                ...state,
                form: {
                    name: action.payload.name === undefined ? state.form.name : action.payload.name,
                    amount: action.payload.amount === undefined ? state.form.amount : action.payload.amount.replace(/\D/g,'')
                },
            }

        default: 
            return state;
    }
}