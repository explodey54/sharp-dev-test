import { AUTHORIZATION_FAIL, LOG_OUT, REMOVE_ERRORS } from '../constants/core'
import {
	USER_INFO_REQUEST,
	USER_INFO_SUCCESS,
	USER_INFO_FAIL,
	USER_TRANSACTIONS_REQUEST,
	USER_TRANSACTIONS_SUCCESS,
	USER_TRANSACTIONS_FAIL,
	CREATE_TRANSACTION_REQUEST,
	CREATE_TRANSACTION_SUCCESS,
	CREATE_TRANSACTION_FAIL,
	SEARCH_USER_REQUEST,
	SEARCH_USER_SUCCESS,
	SEARCH_USER_FAIL,
	TRANSACTION_FORM_SHOW,
    TRANSACTION_FORM_HIDE,
    TRANSACTION_FORM_TOGGLE,
    REPEAT_TRANSACTION_SET,
	REPEAT_TRANSACTION_REMOVE,
	SET_FORM
} from '../constants/main'

import  { userInfoFetch, userTransactionsFetch, createTransactionFetch, searchUserFetch } from '../../services/api'

export function clearErrors() {
	return dispatch => {
		dispatch({
			type: REMOVE_ERRORS,
		})
	}
}

export function logOut() {
	return dispatch => {
		dispatch({
			type: LOG_OUT
		})
		dispatch({
			type: TRANSACTION_FORM_HIDE
		})
		dispatch({
			type: SEARCH_USER_SUCCESS,
			payload: []
		})
		dispatch({
			type: SET_FORM,
			payload: {name: '', amount: ''}
		})
	}
}

export function showTransactionForm() {
	return dispatch => {
		dispatch({
			type: TRANSACTION_FORM_SHOW
		})
	}
}

export function hideTransactionForm() {
	return dispatch => {
		dispatch({
			type: TRANSACTION_FORM_HIDE
		})
	}
}

export function toggleTransactionForm() {
	return dispatch => {
		dispatch({
			type: TRANSACTION_FORM_TOGGLE
		})
	}
}

export function getUserInfo() {
	return (dispatch, getState) => {
		dispatch({
			type: USER_INFO_REQUEST
		})

		const token = getState().core.token

		return userInfoFetch(token)
			.then(response => {
				if (response.ok) {
					return response.json()
				}
				const error = new Error(`${response.status}: ${response.statusText}`)
				error.status = response.status
				throw error
			})
			.then(data => {
				dispatch({
					type: USER_INFO_SUCCESS,
					payload: data.user_info_token
				})
			})
			.catch(error => {
				dispatch({
					type: USER_INFO_FAIL
				})

				if (error.status === 401) {
					dispatch({
						type: AUTHORIZATION_FAIL
					})
				}
			})
	}
}

export function getUserTransactions() {
	return (dispatch, getState) => {
		dispatch({
			type: USER_TRANSACTIONS_REQUEST
		})

		const token = getState().core.token

		return userTransactionsFetch(token)
			.then(response => {
				if (response.ok) {
					return response.json()
				}
				const error = new Error(`${response.status}: ${response.statusText}`)
				error.status = response.status
				throw error
			})
			.then(data => {
				data.trans_token.sort((a,b) => {
					return b.id-a.id
				})
				dispatch({
					type: USER_TRANSACTIONS_SUCCESS,
					payload: data.trans_token
				})
			})
			.catch(error => {
				dispatch({
					type: USER_TRANSACTIONS_FAIL
				})

				if (error.status === 401) {
					dispatch({
						type: AUTHORIZATION_FAIL
					})
				}
			})
	}
}

export function createTransaction(name, amount) {
	return (dispatch, getState) => {
		dispatch({
			type: CREATE_TRANSACTION_REQUEST
		})

		const token = getState().core.token

		return createTransactionFetch(token, name, amount)
			.then(response => {
				if (!response.ok) { throw response }
				return response.json()
			})
			.then(data => {
				dispatch({
					type: CREATE_TRANSACTION_SUCCESS,
					payload: data.trans_token
				})
			})
			.catch(error => {
				error.text().then(errorMessage => {
					if (error.status === 401) {
						dispatch({
							type: AUTHORIZATION_FAIL
						})
						return
					}
					dispatch({
						type: CREATE_TRANSACTION_FAIL,
						payload: errorMessage
					})
				})
			})
	}
}

export function searchUser(name) {
	return (dispatch, getState) => {
		if (getState().containers.main.isLoading) {
			return
		}

		if (!name.trim()) {
			dispatch({
				type: SEARCH_USER_SUCCESS,
				payload: []
			})

			return
		}
		
		dispatch({
			type: SEARCH_USER_REQUEST
		})

		const token = getState().core.token

		return searchUserFetch(token, name)
			.then(response => {
				if (response.ok) {
					return response.json()
				}
				throw new Error(`${response.status}: ${response.statusText}`)
			})
			.then(data => {
				dispatch({
					type: SEARCH_USER_SUCCESS,
					payload: data
				})
			})
			.catch(error => {
				dispatch({
					type: SEARCH_USER_FAIL
				})
			})
	}
}

export function setForm(obj) {
	return dispatch => {
		dispatch({
			type: SET_FORM,
			payload: obj
		})
	}
}

export function setRepeatTransactionInfo(name, amount) {
	return dispatch => {
		dispatch({
			type: TRANSACTION_FORM_SHOW
		})
		dispatch({
			type: REPEAT_TRANSACTION_SET,
			payload: {name: name, amount: Math.abs(amount)}
		})
	}
}

export function removeRepeatTransactionInfo(name, amount) {
	return dispatch => {
		dispatch({
			type: REPEAT_TRANSACTION_REMOVE
		})
	}
}