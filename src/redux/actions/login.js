import { REMOVE_ERRORS } from '../constants/core'
import {
	LOGIN_REQUEST,
    LOGIN_SUCCESS,
    LOGIN_FAIL,
    CREATE_USER_REQUEST,
    CREATE_USER_SUCCESS,
    CREATE_USER_FAIL,
    CHANGE_SHOWN_COMP
} from '../constants/login'
import  { createUserFetch, loginFetch } from '../../services/api'

export function clearErrors() {
	return dispatch => {
		dispatch({
			type: REMOVE_ERRORS,
		})
	}
}

export function changeShownComp(arr) {
	return (dispatch, getState) => {
		dispatch({
			type: CHANGE_SHOWN_COMP,
			payload: arr
		})
	}
}

export function createUser(username, password, email) {
	return dispatch => {
		dispatch({
			type: CREATE_USER_REQUEST
		})

		return createUserFetch(username, password, email)
			.then(response => {
				if (!response.ok) { throw response }
				return response.json()
			})
			.then(data => {
				dispatch({
					type: CREATE_USER_SUCCESS,
					payload: data.id_token
				})
			})
			.catch(error => {
				error.text().then(errorMessage => {
					dispatch({
						type: CREATE_USER_FAIL,
						payload: errorMessage
					})
				})
			})
	}
}

export function login(email, password) {
	return dispatch => {
		dispatch({
			type: LOGIN_REQUEST
		})

		return loginFetch(email, password)
			.then(response => {
				if (!response.ok) { throw response }
				return response.json()
			})
			.then(data => {
				dispatch({
					type: LOGIN_SUCCESS,
					payload: data.id_token
				})
			})
			.catch(error => {
				error.text().then(errorMessage => {
					dispatch({
						type: LOGIN_FAIL,
						payload: errorMessage
					})
				})
			})
	}
}